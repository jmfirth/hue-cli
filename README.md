# hue
A command line tool for controlling Philips Hue lights.

# Installation

```bash
npm install -g babel-node
npm install
```

# Getting Help

On Windows:
```bash
hue help
```

On Mac/Linux:
```bash
./hue.sh help
```

# Usage Example

```bash
# create some groups
hue create group all 1-11
hue create group greatroom 1-3,6
hue create group kitchen 4,5
hue create group den 8,9
hue create group bathroom 10
hue create group accents 7,11

# set up some light colors
hue group greatroom color red
hue group accents color orange
hue group bathroom color orange
hue group kitchen color blue
hue group den color blue

# dim greatroom to 60%
hue group greatroom brightness 60

# set light 3 in the greatroom to yellow
hue light 3 color yellow

# create a new scene from current light state
hue create scene favorite

# turn all lights off
hue group all off

# restore scene
hue scene favorite

# change light 3 to red and save to scene
hue light 3 color red
hue update scene favorite
```
