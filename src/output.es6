export default class Output {
	showMainHelp() {
		console.log(
`
hue, a command line tool for controlling Philips Hue lights.

Help

	hue help

Commands:

	group         -  Group commands.
	light         -  Light commands.
	list          -  List commands.
	update        -  Update commands.
	create        -  Create commands.
	help <topic>  -  Help system.

Type "hue help <command>" for more information.
`
		);
	}

	showGroupHelp() {
		console.log(
`
hue, a command line tool for controlling Philips Hue lights.

Group Help

	hue groups <group_name> <command>

Commands:

	color                    -  Turn group lights on.
	off                      -  Turn group lights off.
	color <color>            -  Set group color (see color list).
	brightness <brightness>  -  Set group brightness (0-100).

Examples:

	hue group greatroom on
	hue group greatroom off
	hue group greatroom color blue
	hue group greatroom brightness 40

Type "hue help" to see the main help information.
`
		);
	}

	showLightHelp() {
		console.log(
`
hue, a command line tool for controlling Philips Hue lights.

Light Help

	hue light <range-list> <command>

Commands:

	color <color>  -  Set group color.

Example:

	hue light 1-3,5 on

Type "hue help" to see the main help information.
`
		);
	}

	showListHelp() {
		console.log(
`
hue, a command line tool for controlling Philips Hue lights.

List Help

	hue list <command>

Commands:

	groups  -  List created groups.
	lights  -  List available lights.
	scenes  -  List known scenes.

Examples:

	hue list lights
	hue list groups

Type "hue help" to see the main help information.
`
		);
	}

	showUpdateHelp() {
		console.log(
`
hue, a command line tool for controlling Philips Hue lights.

Update Help

	hue update <command>

Commands:

	group <name> <range-list>  -  Update the lights in a group.
	scene <name>               -  Update a scene to the current light state.

Examples:

	hue update group greatroom 1-4

Type "hue help" to see the main help information.
`
		);
	}

	showCreateHelp() {
		console.log(
`
hue, a command line tool for controlling Philips Hue lights.

Create Help

	hue create <command>

Commands:

	group <name> <light_pattern>  -  Create a group from a set of lights.
	scene <name> {light_pattern}  -  Create a scene from a set of lights, or use the current state.

Examples:

	hue create group greatroom 1-4
	hue create scene current
	hue create scene scattered 1-3,8,11

Type "hue help" to see the main help information.
`
		);
	}

	showHelpHelp() {
		console.log(
`
hue, a command line tool for controlling Philips Hue lights.

Help Help

	hue help help

Commands:

	group       -  Help and usage of the group subcommand.
	light       -  Help and usage of the light subcommand.
	list        -  Help and usage of the list subcommand.
	update      -  Help and usage of the update subcommand.
	create      -  Help and usage of the create subcommand.
	range-list  -  Help on range lists.

Type "hue help" to see the main help information.
`
		);
	}

	showRangeListHelp() {
		console.log(
`
hue, a command line tool for controlling Philips Hue lights.

Range List Help

	A range list is string specifying a list of light numbers.  Each element in
	the list is seperated by a comma.  An element can be either a number or a
	range.

Elements:

	Number  -  An integer value (e.g. 1, 5)
	Range   -  A range of integers (e.g. 1-3, 7-10)

Examples:

	1           =  [ 1 ]
	1-5         =  [ 1, 2, 3, 4, 5 ]
	1-3,5       =  [ 1, 2, 3, 5 ]
	1-3,5,7-10  =  [ 1, 2, 3, 5, 7, 8, 9, 10 ]

Type "hue help" to see the main help information.
`
		)
	}
}
