import nodeHueApi from "node-hue-api";
import Q from "q";

let HueApi = nodeHueApi.HueApi;
let lightState = nodeHueApi.lightState;

export default class Hue {
	constructor(config, colors) {
		this.config = config;
		this.colors = colors.colors;
		if (!this.config.ip)
			writeIp();
	}

	writeIp() {
		let self = this;
		return this
			.nupnpSearch()
			.then(function (data) {
				self.config.ip = data[0].ipaddress;
				fs.writeFileSync('./config.json', JSON.stringify(self.config));
			});
	}

	writeUser(username) {
		this.config.username = username;
		return fs.writeFileSync('./config.json', JSON.stringify(self.config));
	}

	registerUser(username, description) {
		var self = this;
		var api = new HueApi();
		return HueApi
			.registerUser(this.config.ip, username, description || '')
			.then(function () {
				self.writeUser(username);
			});
	}

	getApi() {
		return new HueApi(this.config.ip, this.config.username);
	}

	getColor(name) {
		for (var index in this.colors) {
			let color = this.colors[index];
			if (color.name === name)
				return color.rgb;
		}
	}

	getGroup(name) {
		let api = this.getApi();
		return api.groups()
			.then(function (groups) {
				var selected = null;
				for (var index in groups) {
					var group = groups[index];
					if (group.name.toLowerCase() === name.toLowerCase()) {
						selected = group;
						break;
					}
				}
				if (!selected) return;
				return selected;
			});
	}

	setLightColor(light, color) {
		let api = this.getApi();
		let rgb = this.getColor(color);
		let state = lightState
			.create()
			.on()
			.rgb(rgb.r, rgb.g, rgb.b);
		return api.setLightState(light, state);
	}

	setLightColors(lights, color) {
		let self = this;
		let api = this.getApi();
		let rgb = this.getColor(color);
		let state = lightState
			.create()
			.on()
			.rgb(rgb.r, rgb.g, rgb.b);
		var chain = lights.reduce(function (previous, light) {
			return previous.then(function () {
				return self.setLightColor(light, color);
			});
		}, Q.resolve('start'));
		return chain;
	}

	setLightBrightnesses(lights, brightness) {
		let self = this;
		let api = this.getApi();
		let state = lightState
			.create()
			.on()
			.brightness(brightness);
		var chain = lights.reduce(function (previous, light) {
			return previous.then(function () {
				return api.setLightState(light, state);
			});
		}, Q.resolve('start'));
		return chain;
	}

	setLightsOn(lights) {
		let self = this;
		let api = this.getApi();
		let state = lightState
			.create()
			.on();
		var chain = lights.reduce(function (previous, light) {
			return previous.then(function () {
				return api.setLightState(light, state);
			});
		}, Q.resolve('start'));
		return chain;
	}

	setLightsOff(lights) {
		let self = this;
		let api = this.getApi();
		let state = lightState
			.create()
			.off();
		var chain = lights.reduce(function (previous, light) {
			return previous.then(function () {
				return api.setLightState(light, state);
			});
		}, Q.resolve('start'));
		return chain;
	}

	setGroupOn(name) {
		let self = this;
		return this
			.getGroup(name)
			.then(function (group) {
				if (!group) return;
				self.setLightsOn(group.lights);
			});
	}

	setGroupOff(name) {
		let self = this;
		return this
			.getGroup(name)
			.then(function (group) {
				if (!group) return;
				self.setLightsOff(group.lights);
			});
	}

	setGroupColor(name, color) {
		let self = this;
		return this
			.getGroup(name)
			.then(function (group) {
				if (!group) return;
				self.setLightColors(group.lights, color);
			});
	}

	setGroupBrightness(name, brightness) {
		let self = this;
		return this
			.getGroup(name)
			.then(function (group) {
				if (!group) return;
				self.setLightBrightnesses(group.lights, brightness);
			});
	}

	listLights() {
		let api = this.getApi();
		return api
			.lights()
			.then(function (lights) {
				console.dir(lights);
			});
	}

	listGroups() {
		let api = this.getApi();
		return api
			.groups()
			.then(function (groups) {
				console.dir(groups);
			});
	}

	listScenes() {
		let api = this.getApi();
		return api
			.scenes()
			.then(function (scenes) {
				console.dir(scenes);
			});
	}

	updateGroup(name, lights) {
		let api = this.getApi();
		return this.getGroup(name)
			.then(function (group) {
				if (!group) return;
				api.updateGroup(group.id, lights);
			});
	}

	createGroup(name, lights) {
		let api = this.getApi();
		return api.createGroup(name, lights);
	}

	getScene(name) {
		let api = this.getApi();
		return api.scenes()
			.then(function (scenes) {
				var selected = null;
				for (var index in scenes) {
					var scene = scenes[index];
					if (scene.name.toLowerCase() === name.toLowerCase()) {
						selected = scene;
						break;
					}
				}
				if (!selected) return;
				return selected;
			});
	}

	createScene(name, lights) {
		if (!lights)
			return this.createSceneCurrent(name);
		let api = this.getApi();
		return api.createScene(lights, name);
	}

	setSceneLightState(name, light, state) {
		let api = this.getApi();
		return this
			.getScene(name)
			.then(function (scene) {
				return api.setSceneLightState(scene.id, light, state);
			});
	}

	createSceneCurrent(name) {
		let self = this;
		let api = this.getApi();
		return this
			// get the current light states
			.getCurrentLightStates()
			// create a new scene with specified name from active lights
			.then(function (lights) {
				var ids = lights.map(function (light) {
					return light.id;
				});
				return self
					.createScene(name, ids)
					.then(function () {
						return lights;
					});
			})
			// set the scene light states
			.then(function (lights) {
				return self.setSceneLightStates(name, lights);
			});
	}

	setSceneLightStates(name, lights) {
		let self = this;
		var chain = lights.reduce(function (previous, light) {
			return previous.then(function () {
				return self.setSceneLightState(name, light.id, light.state);
			}, function (error) {
				console.log('>> failed to set light ', light.id);
			});
		}, Q.resolve('start'));
		return chain;
	}

	getCurrentLightStates() {
		let api = this.getApi();
		let lights = [];
		return api
			.fullState()
			.then(function (state) {
				for (var index in state.lights) {
					var light = state.lights[index];
					light.id = index;
					if (!!light.state.on)
						lights.push(light);
				}
				return lights;
			});
	}

	updateScene(name) {
		let self = this;
		let api = this.getApi();
		return this
			// get the current light states
			.getCurrentLightStates()
			// set the scene light states
			.then(function (lights) {
				return self.setSceneLightStates(name, lights);
			});
	}

	recallScene(name) {
		let api = this.getApi();
		return this
			.getScene(name)
			.then(function (scene) {
				return api.recallScene(scene.id);
			});
	}
}
