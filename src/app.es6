/**
 * hue, a command line tool for controlling Philips Hue lights.
 *
 * Type 'hue help' for usage information.
 *
 * @author Justin Firth <jmfirth@gmail.com>
 */

import fs from "fs";
import processArgv from "minimist";
import Hue from "./Hue";
import Output from "./output";
import RangeParser from "./range-parser";

let config = JSON.parse(fs.readFileSync('./config.json'));
let colors = JSON.parse(fs.readFileSync('./colors.json'));
let hue = new Hue(config, colors);
let argv = processArgv(process.argv.slice(2));
let rangeParser = new RangeParser();
let output = new Output();

switch(argv._[0]) {
	case 'group':
		var name = argv._[1];
		switch(argv._[2]) {
			case 'on':
				hue.setGroupOn(name);
				break;
			case 'off':
				hue.setGroupOff(name);
				break;
			case 'color':
				var color = argv._[3];
				hue.setGroupColor(name, color);
				break;
			case 'brightness':
				var brightness = argv._[3];
				hue.setGroupBrightness(name, brightness);
				break;
			default:
				output.showGroupHelp();
				break;
		}
		break;
	case 'scene':
		var name = argv._[1];
		hue.recallScene(name);
		break;
	case 'light':
		console.log(argv._[1]);
		var lights = rangeParser.parse(argv._[1]);
		switch(argv._[2]) {
			case 'color':
				var color = argv._[3];
				hue.setLightColors(lights, color);
				break;
			default:
				output.showLightHelp();
				break;
		}
	case 'list':
		switch(argv._[1]) {
			case 'lights':
				hue.listLights();
				break;
			case 'groups':
				hue.listGroups();
				break;
			case 'scenes':
				hue.listScenes();
				break;
			default:
				output.showListHelp();
				break;
		}
		break;
	case 'update':
		switch(argv._[1]) {
			case 'group':
				var name = argv._[2];
				var lights = rangeParser.parse(argv._[3]);
				hue.updateGroup(name, lights);
				break;
			case 'scene':
				var name = argv._[2];
				hue.updateScene(name);
				break;
			default:
				output.showUpdateHelp();
		}
		break;
	case 'create':
		switch(argv._[1]) {
			case 'group':
				var name = argv._[2];
				var lights = rangeParser.parse(argv._[3]);
				hue.createGroup(name, lights);
				break;
			case 'scene':
				var name = argv._[2];
				var lights = null;
				if (argv._.length >= 4 && !!argv._[3])
					lights = rangeParser.parse(argv._[3]);
				hue.createScene(name, lights);
				break;
			default:
				output.showCreateHelp();
		}
		break;
	case 'help':
		switch(argv._[1]) {
			case 'group':
				output.showGroupHelp();
				break;
			case 'light':
				output.showLightHelp();
				break;
			case 'list':
				output.showListHelp();
				break;
			case 'update':
				output.showUpdateHelp();
				break;
			case 'create':
				output.showCreateHelp();
				break;
			case 'range-list':
				output.showRangeListHelp();
				break;
			default:
				output.showMainHelp();
				break;
		}
		break;
	default:
		output.showMainHelp();
		break;
}
